define(["./sponsorship-contactroles", "N/record", "N/log"], function (roles, r, log) {

    /**
     * Creates the Presenter Contact for a Sponsor
     *
     * @exports sponsor/add-presenter/wa
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType WorkflowActionScript
     */
    var exports = {};

    /**
     * <code>onAction</code> event handler
     *
     * @governance 0
     *
     * @param context
     *        {Object}
     * @param context.newRecord
     *        {Record} The new record with all changes. <code>save()</code> is not
     *        permitted.
     * @param context.oldRecord
     *        {Record} The old record with all changes. <code>save()</code> is not
     *        permitted.
     *
     * @return {void}
     *
     * @static
     * @function onAction
     */
    function onAction(context) {
        log.audit({title: "Adding Presenter Contact..."});
        addPresenter(context.newRecord);
    }

    /**
     * Associates the Presenter Contact Role to the newly created Contact record
     *
     * @governance 5
     *
     * @param rec {N/record.Record} The Contact record to update
     *
     * @returns {void}
     *
     * @private
     * @function addPresenter
     */
    function addPresenter(rec) {
        var presenterId = rec.getValue({fieldId: "custentity_sponsor_presenter"});
        try {
            r.submitFields({
                type: r.Type.CONTACT,
                id: presenterId,
                values: {
                    "company": rec.id,
                    "contactrole": roles.PRESENTER
                },
                options: {
                    enableSourcing: false
                }
            });
        } catch (e) {
            // Throws an error when duplicate is detected; remove the duplicate
            log.error({title: "Linking Presenter failed; removing duplicate"});
            r.delete({type: r.Type.CONTACT, id: presenterId});
            rec.setValue({value: null, fieldId: "custentity_sponsor_presenter"});
        }
    }

    exports.onAction = onAction;
    return exports;
});
