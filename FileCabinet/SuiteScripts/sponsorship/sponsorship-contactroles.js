define([], function () {

    /**
     * Enumerates the Contact Roles relevant to the Sponsorship module
     *
     * @exports sponsor/contact-roles
     *
     * @readonly
     * @enum {Number}
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     */
    var exports = {};

    /**
     * The Contact responsible for processing payments by a Sponsor
     *
     * @static
     * @property PAYMENT
     */
    exports.PAYMENT = 2;

    /**
     * The Contact responsible for presenting at an Event for a Sponsor
     *
     * @static
     * @property PRESENTER
     */
    exports.PRESENTER = 1;

    /**
     * The Contact responsible for primary communication with a Sponsor
     *
     * @static
     * @property PRIMARY
     */
    exports.PRIMARY = -10;

    return exports;
});
