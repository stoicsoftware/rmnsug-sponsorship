define(["N/record", "N/runtime", "N/log"], function (r, run, log) {

    /**
     * Automatically associates a Sponsor to their requested available RMNSUG Event
     *
     * @exports sponsor/associate-event/wa
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType WorkflowActionScript
     */
    var exports = {};

    /**
     * <code>onAction</code> event handler
     *
     * @governance 2
     *
     * @param context
     *        {Object}
     * @param context.newRecord
     *        {Record} The new record with all changes. <code>save()</code> is not
     *        permitted.
     * @param context.oldRecord
     *        {Record} The old record with all changes. <code>save()</code> is not
     *        permitted.
     *
     * @return {void}
     *
     * @static
     * @function onAction
     */
    function onAction(context) {
        log.audit({title: "Associating Sponsor to Event..."});

        return updateSponsorOnEvent(readData(context.newRecord, run.getCurrentScript()));
    }

    /**
     * Reads all relevant data from necessary sources
     *
     * @governance 0
     *
     * @param rec {N/record.Record} The newly created Sponsor record
     * @param script {N/runtime.Script} The currently executing script
     *
     * @returns data {Object}
     * @returns data.eventId {Number} Internal ID of the RMNSUG Event record to update
     * @returns data.sponsorFieldId {String} Internal ID of the appropriate Sponsor field on the
     *      RMNSUG Event record
     * @returns data.sponsorId {Number} Internal ID of the Sponsor record
     *
     * @private
     * @function readData
     */
    function readData(rec, script) {
        log.audit({title: "Reading Sponsor Data..."});
        return {
            eventId: rec.getValue({fieldId: script.getParameter({name: "custscript_event_field"})}),
            sponsorFieldId: script.getParameter({name: "custscript_sponsor_field"}),
            sponsorId: rec.id
        };
    }

    /**
     * Associates the Sponsor to their requested Event
     *
     * @governance 2
     *
     * @param data {Object}
     * @param data.eventId {Number} Internal ID of the RMNSUG Event record to update
     * @param data.sponsorFieldId {String} Internal ID of the appropriate Sponsor field on the
     *      RMNSUG Event record
     * @param data.sponsorId {Number} Internal ID of the Sponsor record
     *
     * @returns {String} Internal ID of the updated RMNSUG Event record
     *
     * @private
     * @function updateSponsorOnEvent
     */
    function updateSponsorOnEvent(data) {
        log.audit({title: "Updating Event..."});

        // Needs to be instantiated this way due to dynamic nature of key name
        var fieldValues = {};
        fieldValues[data.sponsorFieldId] = data.sponsorId;

        r.submitFields({
            type: "customrecord_hosting",
            id: data.eventId,
            values: fieldValues,
            options: {
                enableSourcing: false
            }
        });

        return data.eventId;
    }

    exports.onAction = onAction;
    return exports;
});
