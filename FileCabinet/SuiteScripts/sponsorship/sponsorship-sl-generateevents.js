define(["N/config", "N/http", "N/log", "N/record", "N/ui/serverWidget", "N/url"], function (config, http, log, r, ui, url) {

    /**
     * Renders a page utilized for expedient creation of RMNSUG Event records
     *
     * @exports sponsor/generate-events/sl
     *
     * @requires N/config
     * @requires N/http
     * @requires N/log
     * @requires N/record
     * @requires N/ui/serverWidget
     * @requires N/url
     *
     * @copyright 2018 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType Suitelet
     */
    var exports = {};

    /**
     * The Events Chair on the Board of Directors
     *
     * @type {string} Internal ID of the Events Chair Board member
     *
     * @private
     * @property eventsChair
     */
    var eventsChair;

    /**
     * <code>onRequest</code> event handler
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.request
     *        {ServerRequest} The incoming request object
     * @param context.response
     *        {ServerResponse} The outgoing response object
     *
     * @return {void}
     *
     * @static
     * @function onRequest
     */
    function onRequest(context) {
        log.audit({title: "Request received..."});

        var requestRouter = {};
        requestRouter[http.Method.GET] = onGet;
        requestRouter[http.Method.POST] = onPost;

        (requestRouter[context.request.method] || onError)(context)
    }

    /**
     * GET request handler
     *
     * @governance 0
     *
     * @param context
     *        {Object}
     * @param context.request
     *        {ServerRequest} The incoming request object
     * @param context.response
     *        {ServerResponse} The outgoing response object
     *
     * @returns {void} Output written directly via context.response
     *
     * @private
     * @function onGet
     */
    function onGet(context) {
        log.audit({title: "Processing GET request..."});
        context.response.writePage({pageObject: renderPage()});
    }

    /**
     * POST request handler
     *
     * @gov 6*n, where n is the number of Events being generated
     *
     * @param context
     *        {Object}
     * @param context.request
     *        {ServerRequest} The incoming request object
     * @param context.response
     *        {ServerResponse} The outgoing response object
     *
     * @returns {String}
     *
     * @private
     * @function onPost
     */
    function onPost(context) {
        log.audit({title: "Processing POST request..."});

        eventsChair = config.load({type: config.Type.COMPANY_PREFERENCES})
            .getValue({fieldId: "custscript_board_events"});

        context.response.writePage({
            pageObject: renderOutput(createEvents(context.request), context.response)
        });
    }

    /**
     * Invalid request type handler
     *
     * @gov 0
     *
     * @param context
     *        {Object}
     * @param context.request
     *        {ServerRequest} The incoming request object
     * @param context.response
     *        {ServerResponse} The outgoing response object
     *
     * @returns {String}
     *
     * @private
     * @function onError
     */
    function onError(context) {
        log.audit({title: "Processing bad request..."});
        context.response.write({output: "ERROR: Invalid request type"});
    }

    /**
     * Renders the initial form used to generate RMNSUG Events
     *
     * @gov 0
     *
     * @returns {N/ui/serverWidget.Form}
     *
     * @private
     * @function renderPage
     */
    function renderPage() {
        log.audit({title: "Rendering form..."});
        var form = ui.createForm({title: "Generate RMNSUG Events"});

        form.clientScriptModulePath = "./sponsorship-cl-generateevents.js";

        var sublist = form.addSublist({
            id: "custpage_eventlist",
            label: "Events",
            type: ui.SublistType.INLINEEDITOR
        });

        sublist.addButton({
            id: "custpage_generateyear",
            label: "Add Full Year",
            functionName: "onGenerateClick"
        });
        sublist.addField({
            id: "custpage_month",
            label: "Event Name (&quot;Month Year&quot;, e.g. &quot;January 2018&quot;)",
            type: ui.FieldType.TEXT
        });

        form.addSubmitButton({label: "Generate Events"});

        return form;
    }

    /**
     * Reads Event data from the request and creates the RMNSUG Event records
     *
     * @gov 6*n, where n is the number of Events being generated
     *
     * @param request {N/http.Request} HTTP request data
     *
     * @returns {Object[]} List of Internal IDs and Names of newly created records
     *
     * @private
     * @function createEvents
     */
    function createEvents(request) {
        log.audit({title: "Creating Event records..."});

        var lineCount = request.getLineCount({group: "custpage_eventlist"});
        var eventData = [];

        for (var i = 0; i < lineCount; i++) {
            var eventName = request.getSublistValue({
                group: "custpage_eventlist",
                name: "custpage_month",
                line: i
            });
            eventData.push({
                name: eventName,
                internalid: lineToRecord(eventName)
            });
        }

        return eventData;
    }

    /**
     * Creates a new RMNSUG Event record using the given month as its Name
     *
     * @gov 6
     *
     * @param month {String} The Name of the Event being generated
     *
     * @returns {String} The internal ID of the newly created RMNSUG Event record
     *
     * @private
     * @function lineToRecord
     */
    function lineToRecord(month) {
        return r.create({type: "customrecord_hosting"})
            .setValue({fieldId: "name", value: month})
            .setValue({fieldId: "custrecord_event_host", value: eventsChair})
            .save();
    }

    /**
     * Renders the List page after successful Event generation
     *
     * @gov 0
     *
     * @param ids {String[]} List of internal IDs of generated records
     * @param response {N/http.Response} HTTP Response Object
     *
     * @returns {N/ui/serverWidget.List}
     *
     * @private
     * @function renderOutput
     */
    function renderOutput(data, response) {
        log.audit({title: "Rendering List..."});

        var list = ui.createList({title: "Successfully created Events:"});

        list.addColumn({
            id: "name",
            type: ui.FieldType.URL,
            label: "RMNSUG Event Record"
        }).setURL({
            url: getBaseUrl()
        }).addParamToURL({
            param: "id",
            value: "internalid",
            dynamic: true
        });

        log.debug({title: "data", details: data});
        list.addRows(data);

        return list;
    }

    /**
     * Generates the base URL for an RMNSUG Event record
     *
     * @gov 0
     *
     * @returns {String} The base URL for an RMNSUG Event record
     *
     * @private
     * @function getBaseUrl
     */
    function getBaseUrl() {
        return url.resolveRecord({recordType: "customrecord_hosting"});
    }

    exports.onRequest = onRequest;
    return exports;
});
