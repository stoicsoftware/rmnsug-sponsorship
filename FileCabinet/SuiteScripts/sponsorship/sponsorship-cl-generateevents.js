define(["N/currentRecord"], function (cr) {

    /**
     * Client-side functionality for the RMNSUG Event generation Suitelet
     *
     * @exports sponsor/generate-events/cl
     *
     * @requires N/currentRecord
     *
     * @copyright 2018 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType ClientScript
     */
    var exports = {};

    /**
     * The months that constitute a full year of RMNSUG Events
     *
     * @type {string[]}
     *
     * @private
     * @property months
     */
    var months = [
        "January",
        "March",
        "May",
        "July",
        "September",
        "November"
    ];

    /**
     * Click handler for Generate Year button
     *
     * @gov 0
     *
     * @returns {void}
     *
     * @static
     * @function onGenerateClick
     */
    function onGenerateClick() {
        var year = parseInt(prompt("What Year?")) || 0;

        if (!year) { return; }

        months.map(monthToName(year)).forEach(nameToLine);
    }

    /**
     * Iterator method used for concatenating Month and Year text
     *
     * <em>Curried</em>
     *
     * @gov 0
     *
     * @param year {String} The Year being generated
     *
     * @returns {String} Concatenated month and year to be used as name of Event
     *
     * @private
     * @function monthToName
     */
    function monthToName(year) {
        return function (month) {
            return month + " " + year;
        }
    }

    /**
     * Iterator method used to add a Name to the Event sublist
     *
     * @gov 0
     *
     * @param name {String} The Event name
     *
     * @returns {void}
     *
     * @private
     * @function nameToLine
     */
    function nameToLine(name) {
        var rec = cr.get();
        rec.selectNewLine({sublistId: "custpage_eventlist"});
        rec.setCurrentSublistValue({
            sublistId: "custpage_eventlist",
            fieldId: "custpage_month",
            value: name
        });
        rec.commitLine({sublistId: "custpage_eventlist"});
    }

    function pageInit(context) { /* Empty, required by NetSuite */ }

    exports.onGenerateClick = onGenerateClick;
    exports.pageInit = pageInit;
    return exports;
});
