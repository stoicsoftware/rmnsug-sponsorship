define([
    "/SuiteScripts/lib/moment.min",
    "N/record",
    "N/runtime",
    "N/search"
], function (moment, r, run, s) {

    /**
     * Creates an Invoice for a new Sponsorship request
     *
     * @exports sponsor/create-invoice/wa
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType WorkflowActionScript
     */
    var exports = {};

    /**
     * <code>onAction</code> event handler
     *
     * @governance 12
     *
     * @param context
     *        {Object}
     * @param context.newRecord
     *        {Record} The new record with all changes. <code>save()</code> is not
     *        permitted.
     * @param context.oldRecord
     *        {Record} The old record with all changes. <code>save()</code> is not
     *        permitted.
     *
     * @return {String} Internal ID of the newly created Invoice
     *
     * @static
     * @function onAction
     */
    function onAction(context) {
        log.audit({title: "Creating Invoice for new Sponsorship request..."});

        return createInvoice(readData(context.newRecord, run.getCurrentScript()));
    }

    /**
     * Reads all relevant data from necessary sources
     *
     * @governance 2
     *
     * @param rec {N/record.Record} The newly created Sponsor record
     * @param script {N/runtime.Script} The currently executing script
     *
     * @returns data {Object}
     * @returns data.itemId {String} The internal ID of the Sponsorship Item to add
     *      to the Invoice
     * @returns data.sponsorId {String} The internal ID of the Sponsor record
     * @returns data.eventId {String} The internal ID of the Event record
     * @returns data.eventName {String} The name of the Event record
     *
     * @private
     * @function readData
     */
    function readData(rec, script) {
        log.audit({title: "Reading Sponsor Data..."});

        var eventId = script.getParameter({name: "custscript_sponsor_event"});

        return {
            itemId: script.getParameter({name: "custscript_sponsor_item"}),
            sponsorId: rec.id,
            eventId: eventId,
            eventName: s.lookupFields({
                type: "customrecord_hosting",
                id: eventId,
                columns: ["name"]
            }).name
        };
    }

    /**
     * Reads all relevant data from necessary sources
     *
     * @governance 10
     *
     * @param data {Object}
     * @param data.itemId {String} The internal ID of the Sponsorship Item to add
     *      to the Invoice
     * @param data.sponsorId {String} The internal ID of the Sponsor record
     *
     * @returns {String} Internal ID of the newly created Invoice
     *
     * @private
     * @function readData
     */
    function createInvoice(data) {
        log.audit({title: "Creating Invoice..."});

        var invoice = r.create({
            type: r.Type.INVOICE,
            defaultValues: {
                entity: data.sponsorId
            }
        });

        invoice.setValue({fieldId: "tobeemailed", value: false});
        invoice.setValue({fieldId: "custbody_sponsor_event", value: data.eventId});
        invoice.setValue({fieldId: "memo", value: data.eventName});
        invoice.setValue({fieldId: "trandate", value: generateDate(data.eventName)});
        invoice.setValue({fieldId: "class", value: findClass(data.eventName)});

        invoice.setSublistValue({
            sublistId: "item",
            fieldId: "item",
            line: 0,
            value: data.itemId
        });

        return invoice.save();
    }

    function generateDate(name) {
        // name expected as "Month YYYY" e.g. "November 2020"
        var date = name.split(/\s/); // => ["November", "2020"]
        return moment()
            .year(date[1])
            .month(date[0]) // Set to meeting month/year
            .startOf("month") // Shift date to 1st of month
            .subtract(3, "months") // Shift back 3 months to allow time for payment
            .toDate(); // Convert to Date for setValue
    }

    function findClass(name) {
        var result = s.create({
            type: s.Type.CLASSIFICATION,
            filters: [
                ["isinactive", s.Operator.IS, false], "AND",
                ["name", s.Operator.IS, generateClass(name)]
            ]
        }).run().getRange({start: 0, end: 1})[0];

        return result ? result.id : 0;
    }

    function generateClass(name) {
        // name expected as "Month YYYY" e.g. "November 2020"
        var date = name.split(/\s/); // => ["November", "2020"]
        return moment()
            .year(date[1])
            .month(date[0]) // Set to meeting month/year
            .format("YY.MM");
    }

    exports.onAction = onAction;
    return exports;
});
