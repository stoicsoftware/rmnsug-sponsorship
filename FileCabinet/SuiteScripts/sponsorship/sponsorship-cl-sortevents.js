var sponsor = sponsor || {};
sponsor.events = (function () {

    /**
     * Sorts the available Events list on the Online Sponsorship Request form
     *
     * @exports sponsor/sort-events/cl
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     */
    var exports = {};

    /**
     * <code>pageInit</code> event handler
     *
     * @governance 0
     *
     * @param context
     *        {Object}
     * @param context.mode
     *        {String} The access mode of the current record. Will be one of
     *            <ul>
     *            <li>copy</li>
     *            <li>create</li>
     *            <li>edit</li>
     *            </ul>
     *
     * @return {void}
     *
     * @static
     * @function pageInit
     */
    function pageInit(context) {
        // XXX jQuery currently available globally but may be worth
        // including our own module for it instead
        var listSelector = retrieveEventField();

        if (!listSelector) { return; }

        var list = jQuery(listSelector);
        var selected = list.val();

        var sortedOptions = jQuery(listSelector + " option").sort(sortByValue);
        list.empty().append(sortedOptions);
        list.val(selected);
    }

    /**
     * Determines which select field is available as this changes based on the form loaded
     *
     * @governance 0
     *
     * @returns {String} The id attribute of the Events list, or an empty String if none
     *
     * @private
     * @function retrieveEventField
     */
    function retrieveEventField() {
       if (jQuery("#custentity_sponsor_availablebreakfast").length == 1) {
         return "#custentity_sponsor_availablebreakfast";
       }
       if (jQuery("#custentity_sponsor_availablesuiteapp").length == 1) {
         return "#custentity_sponsor_availablesuiteapp";
       }      
    }

    /**
     * Sorting function for list option elements. Sorts by value in ascending order
     *
     * @governance 0
     *
     * @param a {option} First option to compare
     * @param b {option} Second option to compare
     *
     * @returns {Number} Sort indicator for the given elements
     *
     * @private
     * @function sortByValue
     */
    function sortByValue(a, b) {
        return (parseInt(a.value) - parseInt(b.value));
    }

    exports.pageInit = pageInit;
    return exports;
})();
