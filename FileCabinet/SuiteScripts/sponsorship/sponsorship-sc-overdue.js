define(["N/email", "N/render", "N/runtime", "N/search", "N/log"], function (email, render, run, s, log) {

    /**
     * Sends email notifications for any overdue Sponsorship Invoices
     *
     * @exports sponsor/overdue-invoice/sc
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType ScheduledScript
     */
    var exports = {};

    /**
     * <code>execute</code> event handler
     *
     * @governance <code>10*(x+1)</code> where x is the number of overdue invoices found
     *
     * @param context
     *        {Object}
     * @param context.type
     *        {InvocationTypes} Enumeration that holds the string values for
     *            scheduled script execution contexts
     *
     * @return {void}
     *
     * @static
     * @function execute
     */
    function execute(context) {
        log.audit({title: "Starting Overdue Invoice Notifications..."});

        var parameters = readParams(run.getCurrentScript());
        findOverdueInvoices(parameters.daysAfterDue).each(sendNotification(parameters));
    }

    /**
     * Reads relevant values from Script Parameters
     *
     * @governance 0
     *
     * @param script {N/runtime.Script} Currently executing Script instance
     *
     * @returns data {Object}
     * @returns data.daysAfterDue {Number} Number of days overdue that triggers notification
     * @returns data.templateId {String} Internal ID of email template to use for overdue notifications
     *
     * @private
     * @function readParams
     */
    function readParams(script) {
        return {
            authorId: script.getParameter({name: "custscript_overdue_sender"}),
            daysAfterDue: script.getParameter({name: "custscript_overdue_days"}),
            sponsorChair: script.getParameter({name: "custscript_board_sponsor"}),
            templateId: script.getParameter({name: "custscript_overdue_template"})
        };
    }

    /**
     * Finds all overdue Invoices
     *
     * @governance 0
     *
     * @param daysAfterDue {Number} Number of days overdue that triggers notification
     *
     * @returns {N/search.ResultSet} Search object containing Invoice Results
     *
     * @private
     * @function findOverdueInvoices
     */
    function findOverdueInvoices(daysAfterDue) {
        log.audit({title: "Finding overdue invoices..."});

        return s.create({
            type: s.Type.INVOICE,
            filters: [
                ["mainline", s.Operator.IS, true],
                "and", ["status", s.Operator.IS, "CustInvc:A"],
                "and", ["formulanumeric:FLOOR({today}-{duedate})", s.Operator.EQUALTO, daysAfterDue]
            ],
            columns: ["entity"]
        }).run();
    }

    /**
     * Merges the given result with the provided templateId and sends the resulting email notification
     *
     * <em>Curried</em>
     *
     * @governance 0
     *
     * @param data {Object}
     * @param data.authorId {String} Internal ID of the Employee to use as the sender for
     *      overdue notifications
     * @param data.sponsorChair {String} Internal ID of the Sponsorship Chair Employee
     * @param data.templateId {String} Internal ID of the email template to use for overdue notifications
     *
     * @returns {Function} Partially-applied function for merging a template and a search result
     *
     * @private
     * @function sendNotification
     */
    function sendNotification(data) {

        /**
         * Fully applies the sendNotification function by supplying the search result
         *
         * @governance 10
         *
         * @param result {N/search.Result} Search Result representing an overdue Invoice
         *
         * @returns {Boolean} <code>true</code> to continue iterating to the next result;
         *      <code>false</code> otherwise
         */
        return function(result) {
            log.audit({title: "Sending overdue notification..."});

            var recipientId = result.getValue({name: "entity"});
            var invoiceId = parseInt(result.id, 10);

            var pdf = render.transaction({
                entityId: invoiceId,
                printMode: render.PrintMode.PDF
            });

            var mergeResult = render.mergeEmail({
                templateId: data.templateId,
                transactionId: invoiceId
            });

            email.send({
                author: data.authorId,
                recipients: recipientId,
                cc: [data.sponsorChair],
                subject: mergeResult.subject,
                body: mergeResult.body,
                attachments: [pdf],
                relatedRecords: {
                    entityId: recipientId,
                    transactionId: result.id
                }
            });

            return true;
        }
    }

    exports.execute = execute;
    return exports;
});
