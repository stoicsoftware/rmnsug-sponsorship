define(["N/email", "N/render", "N/runtime"], function (email, render, run) {

    /**
     * Workflow Action that sends confirmation email to Sponsor
     *
     * @exports sponsor/notify-contact/wa
     *
     * @copyright 2018 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NScriptType WorkflowActionScript
     */
    var exports = {};

    /**
     * <code>onAction</code> event handler
     *
     * @gov 10
     *
     * @param context
     *        {Object}
     * @param context.newRecord
     *        {Record} The new record with all changes. <code>save()</code> is not
     *        permitted.
     * @param context.oldRecord
     *        {Record} The old record with all changes. <code>save()</code> is not
     *        permitted.
     *
     * @return {void}
     *
     * @static
     * @function onAction
     */
    function onAction(context) {
        log.audit({title: "Sending sponsorship confirmation email..."});

        var params = readParameters(run.getCurrentScript());
        log.debug({title: "params", details: params});

        log.audit({title: "Generating template..."});
        var template = render.mergeEmail({
            templateId: params.templateId,
            customRecord: {
                type: "customrecord_hosting",
                id: params.eventId
            }
        });

        log.audit({title: "Generating email..."});
        email.send({
            author: params.sender,
            recipients: [params.recipient],
            subject: template.subject,
            body: template.body,
            relatedRecords: {
                customRecord: {
                    recordType: "customrecord_hosting",
                    id: params.eventId
                }
            }
        });

        log.audit({title: "Confirmation sent."});
    }

    function readParameters(script) {
        return {
            eventId: parseInt(script.getParameter({name: "custscript_sponsor_confirmevent"})),
            recipient: script.getParameter({name: "custscript_sponsor_confirmrecipient"}),
            sender: script.getParameter({name: "custscript_sponsor_confirmsender"}),
            templateId: parseInt(script.getParameter({name: "custscript_sponsor_confirmtemplate"}))
        };
    }

    exports.onAction = onAction;
    return exports;
});
