## RMNSUG Sponsorship Automation

Automates the acceptance, invoicing, and tracking of meeting sponsorship for the [Rocky Mountain NetSuite User Group](http://www.rmnsug.org/).
